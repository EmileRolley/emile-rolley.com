/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.elm"],
  theme: {
    extend: {
      animation: {
        "bounce-slow": "bounce 1s infinite",
        "bounce-veryslow": "bounce 4s infinite",
      },
      fontFamily: {
        mono: ["JetBrains Mono", "monospace"],
        serif: ["Averia Serif Libre", "serif"],
      },
      dropShadow: {
        "4xl": ["-10px 50px 40px rgba(0, 0, 0, 1.0)"],
      },
    },
  },
  plugins: [require("@tailwindcss/typography")],
};
