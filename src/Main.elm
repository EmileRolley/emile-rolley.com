port module Main exposing (Msg(..), main, update)

-- import Html.Lazy exposing (lazy)

import Browser exposing (Document)
import Browser.Navigation as Nav
import CodeSnippet
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Attributes.Aria exposing (ariaLabel)
import Html.Events exposing (..)
import Icons
import Json.Decode as Json
import Markdown
import Project exposing (Project(..))
import ScrollTo
import Url exposing (Url)



-- Model


type alias Model =
    { project : Project
    , key : Nav.Key
    , scrollTo : ScrollTo.State
    }


init : () -> Url -> Nav.Key -> ( Model, Cmd msg )
init _ url key =
    ( { project = Project.fromUrl url
      , key = key
      , scrollTo = ScrollTo.init
      }
    , Cmd.none
    )



-- Main


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        , onUrlRequest = LinkedClicked
        , onUrlChange = UrlChanged
        }



-- Update


type Msg
    = LinkedClicked Browser.UrlRequest
    | UrlChanged Url
    | ScrollToMsg ScrollTo.Msg
    | ScrollToId String
    | ScrollInstantToId String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LinkedClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            ( { model | project = Project.fromUrl url }, Cmd.none )

        ScrollToMsg scrollToMsg ->
            let
                ( scrollToModel, scrollToCmds ) =
                    ScrollTo.update scrollToMsg model.scrollTo
            in
            ( { model | scrollTo = scrollToModel }
            , Cmd.map ScrollToMsg scrollToCmds
            )

        ScrollToId id ->
            ( model
            , Cmd.map ScrollToMsg (ScrollTo.scrollTo id)
            )

        ScrollInstantToId id ->
            ( model
            , simpleScrollTo id
            )


port simpleScrollTo : String -> Cmd msg



-- Subscriptions


subscriptions : Model -> Sub Msg
subscriptions model =
    ScrollTo.subscriptions model.scrollTo
        |> Sub.map ScrollToMsg



-- View


view : Model -> Document Msg
view model =
    { title = "Emile Rolley"
    , body =
        [ viewBody model ]
    }


viewBody : Model -> Html Msg
viewBody model =
    div
        [ class "bg-black" ]
        [ viewHero
        , div [ id "projects", class "flex flex-col px-[5vw] pb-[5vh] 2xl:px-[7vw] min-h-screen" ]
            [ div [ class "flex flex-col xl:flex-row gap-8 items-baseline sticky top-0 py-8 bg-black z-10 xl:z-0" ]
                [ h2 [ class "text-white text-5xl font-serif" ] [ text "Projets" ]
                , viewNav model.project
                ]
            , viewProject model.project
            ]
        ]


viewHero : Html Msg
viewHero =
    div [ class "flex flex-col h-screen w-full justify-between items-center pt-[10vh] pb-[10vh]" ]
        [ div [ class "flex flex-col h-full gap-8 justify-center items-center" ]
            [ h1 [ class "text-white text-center text-6xl sm:text-8xl font-serif" ]
                [ text "Emile Rolley" ]
            , p [ class "text-white font-serif p-8 text-center italic text-xl sm:text-2xl max-w-6xl leading-relaxed" ]
                [ text """
            I'm interested in programming languages and the open-source.
            I want to build sustainable and aesthetic pieces of software that have a meaningful impact on all living beings.
            """
                ]
            , viewContact
            ]
        , viewCTA
        ]


viewContact : Html msg
viewContact =
    let
        aClass =
            "cursor-pointer hover:text-pink-500 active:text-pink-800"
    in
    div [ class "flex gap-4 sm:gap-8 text-white font-serif text-xl sm:text-2xl mt-24" ]
        [ a
            [ class aClass
            , href "https://github.com/EmileRolley"
            , target "_blank"
            ]
            [ text "GitHub" ]
        , a
            [ class aClass
            , href "https://www.linkedin.com/in/emile-rolley-703b82206/"
            , target "_blank"
            ]
            [ text "LinkedIn" ]
        , a
            [ class aClass
            , href "mailto:emile.rolley@tuta.io"
            ]
            [ text "Email" ]
        , a
            [ class aClass
            , href "https://photos.emile-rolley.com"
            ]
            [ text "Photography" ]
        ]


viewCTA : Html Msg
viewCTA =
    button
        [ class "animate-pulse transition-all ease-out duration-300 text-black sm:text-white w-10 sm:w-8 sm:mt-2 text-7xl font-mono content-end cursor-pointer bg-white rounded-full hover:animate-none hover:text-black hover:w-10 hover:mt-0 active:w-12"
        , onClick (ScrollToId "projects")
        , id "cta"
        , ariaLabel "Scroll to projects"
        ]
        [ div [ class "px-2 py-2 active:pt-3" ] [ Icons.arrowDown ]
        ]


onClickWithPreventDefault : msg -> Attribute msg
onClickWithPreventDefault msg =
    preventDefaultOn "click" (Json.map alwaysPreventDefault (Json.succeed msg))


alwaysPreventDefault : msg -> ( msg, Bool )
alwaysPreventDefault msg =
    ( msg, True )


viewNav : Project -> Html Msg
viewNav currentProject =
    div
        [ class "overflow-x-auto flex gap-4 max-w-full text-white font-serif font-semibold text-2xl" ]
        (Project.all
            |> List.map
                (\project ->
                    let
                        infos =
                            Project.getInfos project

                        isCurrent =
                            currentProject == project

                        id =
                            Project.toString project
                    in
                    div []
                        [ a
                            [ class
                                "cursor-pointer hover:text-pink-500 active:text-pink-800"
                            , href ("/?p=" ++ id)
                            , onClickWithPreventDefault (ScrollInstantToId "projects")
                            ]
                            [ div [ class "flex flex-col flex-no-wrap items-center italic" ]
                                [ text infos.name
                                , if isCurrent then
                                    div
                                        [ class "bg-pink-500 rounded-full w-1 h-1" ]
                                        []

                                  else
                                    text ""
                                ]
                            ]
                        ]
                )
        )


viewProject : Project -> Html Msg
viewProject project =
    div [ class "pt-8" ]
        (Project.all
            |> List.map
                (\p ->
                    let
                        isVisible =
                            p == project

                        projectInfos =
                            Project.getInfos p
                    in
                    div
                        [ class
                            ("transition-all ease-in duration-100 flex justify-between flex-col xl:flex-row gap-8 2xl:gap-14 z-0 "
                                ++ (if isVisible then
                                        "  opacity-100"

                                    else
                                        " opacity-0"
                                   )
                            )
                        ]
                        (if isVisible then
                            [ div
                                [ class "prose  text-xl prose-invert text-slate-50 max-w-fit font-serif text-lg hyphens-auto prose-a:font-bold prose-a:text-white prose-a:no-underline hover:prose-a:text-pink-500 active:prose-a:text-pink-800"
                                , lang "fr"
                                ]
                                (Markdown.toHtml Nothing projectInfos.body)
                            , div [ class "xl:max-w-[50%]" ]
                                [ div [ class "sticky top-28" ]
                                    [ CodeSnippet.view projectInfos.codeSnippet
                                    ]
                                ]
                            ]

                         else
                            []
                        )
                )
        )
