module Icons exposing (..)

import Html exposing (..)
import Html.Attributes exposing (src)
import Svg exposing (svg)
import Svg.Attributes exposing (..)



-- <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-chevrons-down"><path d="m7 6 5 5 5-5"/><path d="m7 13 5 5 5-5"/></svg>


chevronsDown : Html msg
chevronsDown =
    svg
        [ viewBox "0 0 24 24"
        , fill "none"
        , stroke "currentColor"
        , strokeWidth "1"
        , strokeLinecap "round"
        , strokeLinejoin "round"
        ]
        [ Svg.path
            [ d "m7 6 5 5 5-5"
            ]
            []
        , Svg.path
            [ d "m7 13 5 5 5-5"
            ]
            []
        ]



-- <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-chevron-down"><path d="m6 9 6 6 6-6"/></svg>


chevronDown : Html msg
chevronDown =
    svg
        [ viewBox "0 0 24 24"
        , fill "none"
        , stroke "currentColor"
        , strokeWidth "2"
        , strokeLinecap "round"
        , strokeLinejoin "round"
        ]
        [ Svg.path
            [ d "m6 9 6 6 6-6"
            ]
            []
        ]



-- <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-arrow-down"><path d="M12 5v14"/><path d="m19 12-7 7-7-7"/></svg>


arrowDown : Html msg
arrowDown =
    svg
        [ viewBox "0 0 24 24"
        , fill "none"
        , stroke "currentColor"
        , strokeWidth "2"
        , strokeLinecap "round"
        , strokeLinejoin "round"
        ]
        [ Svg.path
            [ d "M12 5v14"
            ]
            []
        , Svg.path
            [ d "m19 12-7 7-7-7"
            ]
            []
        ]
