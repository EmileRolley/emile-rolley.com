module Project exposing (Project(..), all, fromUrl, getInfos, toString)

import Projects.Catala as Catala
import Projects.Ekofest as Ekofest
import Projects.Infos exposing (ProjectInfos)
import Projects.NosGestesClimat as NosGestesClimat
import Projects.Publicodes as Publicodes
import Url
import Url.Parser exposing (..)
import Url.Parser.Query as Query



-- Project


type Project
    = Ekofest
    | Catala
    | NosGestesClimat
    | Publicodes


all : List Project
all =
    [ Ekofest, Publicodes, Catala, NosGestesClimat ]


getInfos : Project -> ProjectInfos
getInfos project =
    case project of
        Ekofest ->
            Ekofest.infos

        Catala ->
            Catala.infos

        NosGestesClimat ->
            NosGestesClimat.infos

        Publicodes ->
            Publicodes.infos



-- URL parser


parser : Parser (Maybe String -> a) a
parser =
    top <?> Query.string "p"


{-| TODO: to refactor
-}
fromUrl : Url.Url -> Project
fromUrl url =
    case Url.Parser.parse parser url of
        Just (Just "ekofest") ->
            Ekofest

        Just (Just "catala") ->
            Catala

        Just (Just "nos-gestes-climat") ->
            NosGestesClimat

        Just (Just "publicodes") ->
            Publicodes

        _ ->
            -- Default project
            Ekofest


toString : Project -> String
toString project =
    case project of
        Ekofest ->
            "ekofest"

        Catala ->
            "catala"

        NosGestesClimat ->
            "nos-gestes-climat"

        Publicodes ->
            "publicodes"
