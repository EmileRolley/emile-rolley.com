module Projects.Catala exposing (infos)

import CodeSnippet
import Projects.Infos exposing (ProjectInfos)


infos : ProjectInfos
infos =
    { name = "Catala"
    , body = body
    , codeSnippet = codeSnippet
    }


body : String
body =
    """
[Catala](https://catala-lang.org) est un langage de programmation dédié à
l'implémentation d'algorithmes issus de textes législatifs. Il repose sur une
méthode de programmation en binôme avec des juristes et des développeureuses.
Pour ce faire, les programmes sont rédigés dans un style littéraire où le texte
législatif est annoté par des blocs de code. Ces programmes peuvent ensuite
être compilés vers différents langages de programmation en fonction des besoins
: OCaml, Python, JavaScript, etc...

Le projet a été initié par [Denis Merigoux](https://merigoux.ovh) au cours de
sa
[thèse](https://theses.hal.science/tel-03622012/file/Merigoux_2021_These.pdf)
et est développé au sein de l'équipe
[PROSECCO](https://prosecco.gforge.inria.fr) de l'INRIA Paris par une équipe
pluridisciplinaire et internationale d'une dizaine de
[personnes](https://catala-lang.org/fr/about).

## Contributions

### Génération automatisé d'explications individuelles

Pendant un stage de M1 (de mai à août 2022) ainsi que dans le cadre d'une
mission freelance (année 2023, financée par la [Direction interministérielle du
numérique](https://www.numerique.gouv.fr/)), j'ai travaillé sur la génération
automatisée d'explications individuelles à partir de programmes Catala.

J'ai ainsi développé un prototype  qui est accessible sur
[code.gouv.fr/demos/catala](https://code.gouv.fr/demos/catala/). Pour ce faire,
j'ai dû étendre le compilateur Catala avec un nouveau backend pour générer
toutes les interfaces nécessaires à la création de simulateurs webs à partir du
code source Catala, ainsi que développer un module de génération d'explications
au format DOCX :
[`catala-explain`](https://github.com/CatalaLang/catala-explain).

Le contexte et le résultat de ce travail est présenté dans le rapport de
recherche : [_De la transparence à l’explicabilité automatisée des algorithmes
: comprendre les obstacles informatiques, juridiques et
organisationnels_](https://inria.hal.science/hal-04391612), par Denis Merigoux,
Marie Alauzen, Justine Banuls, Louis Gesbert, Émile Rolley.

### Contributions open-source

Je contribue ponctuellement et bénévolement à l'ensemble du projet depuis mars
2021. En particulier, j'ai effectué une refonte de la syntaxe du langage pour
être compatible avec le format Markdown ainsi que de
[Clerk](https://github.com/CatalaLang/catala/blob/master/build_system/README.md)
(le _build system_ de Catala) pour utiliser [`ninja`](https://ninja-build.org/).
Je me suis également occupé de la refonte du site web de Catala et de la
maintenance des runtimes et backends web de Catala.

---

## Ressources

### Site web

- [catala-lang.org](https://catala-lang.org)
- [code.gouv.fr/demos/catala](https://code.gouv.fr/demos/catala)

### Code source

- [CatalaLang/catala](https://github.com/CatalaLang/catala)
- [CatalaLang/catala-explain](https://github.com/CatalaLang/catala-explain)
- [CatalaLang/catala-dsfr](https://github.com/CatalaLang/catala-dsfr)
- [codegouvfr/rescript-react-dsfr](https://github.com/codegouvfr/rescript-react-dsfr)
"""


codeSnippet : CodeSnippet.Infos
codeSnippet =
    { file = "catala-example/aides_logement/code_construction_legislatif.catala_fr"
    , href = "https://github.com/CatalaLang/catala-examples/blob/518ca1b95fa06792d2526d95f061582a1e892bc5/aides_logement/code_construction_legislatif.catala_fr#L113-L152"
    , code =
        """###### Article L822-2 | LEGIARTI000038814944

I.-Peuvent bénéficier d'une aide personnelle au logement :

1° Les personnes de nationalité française ;

2° Les personnes de nationalité étrangère remplissant les conditions
prévues par les deux premiers alinéas de l' article L. 512-2 du
code de la sécurité sociale .

```catala
champ d'application ÉligibilitéAidesPersonnelleLogement:
  définition condition_nationalité égal à
    selon demandeur.nationalité sous forme
    -- Française: vrai
    -- Étrangère de conditions:
      conditions.satisfait_conditions_l512_2_code_sécurité_sociale
```

II.-Parmi les personnes mentionnées au I, peuvent bénéficier d'une aide
personnelle au logement celles remplissant les conditions prévues par le
présent livre pour son attribution qui sont locataires, résidents en
logement-foyer ou qui accèdent à la propriété d'un local à usage exclusif
d'habitation et constituant leur résidence principale.

Les sous-locataires, sous les mêmes conditions, peuvent également en bénéficier.

```catala
champ d'application ÉligibilitéAidesPersonnelleLogement:
  étiquette l822_2 règle condition_logement_mode_occupation sous condition
    selon ménage.logement.mode_occupation sous forme
      -- Locataire: vrai
      -- RésidentLogementFoyer: vrai
      -- AccessionPropriétéLocalUsageExclusifHabitation:
        ménage.logement.résidence_principale
      -- SousLocataire: vrai
      -- LocationAccession: vrai # Justifié par L831-2, avec une
                                 # exception à venir
  conséquence rempli
```
"""
    }
