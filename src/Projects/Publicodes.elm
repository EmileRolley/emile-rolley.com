module Projects.Publicodes exposing (infos)

import CodeSnippet
import Projects.Infos exposing (ProjectInfos)


infos : ProjectInfos
infos =
    { name = "Publicodes"
    , body = body
    , codeSnippet = codeSnippet
    }


body : String
body =
    """
[Publicodes](https://publi.codes) est un langage de modélisation de domaines
métiers. Il permet de facilement définir un ensemble de règle de calculs
élémentaires et de les combiner pour obtenir des résultats plus complexes. La
syntaxe est en français pour faciliter la compréhension des règles par des
personnes non-techniques.

Il a été créé par [Maël Thomas]() pour le site
[mon-entreprise.fr](https://mon-entreprise.fr) afin de faciliter le calcul des
cotisations sociales et des impôts pour les entrepreneurs. Le langage est à
présent principalement utilisé dans des produits issus de l'incubateur
[beta.gouv.fr](https://beta.gouv.fr).

## Contributions

J'ai contribué à ce projet dans le câdre d'une mission de développeur
indépendant sur [Nos Gestes Climat](/?=nos-gestes-climat) de septembre
2022 à février 2024.
A présent, je fais partie des mainteneurs principaux du projet et continue le
développement bénévolement.

### Système d'import et optimisation à la compilation

Ma principale contribution a été la création du paquet
[`@publicodes/tools`](https://github.com/publicodes/tools) visant à faciliter
l'implémentation et la maintenance des modèles publicodes.

Ce paquet ajoute un système d'import de règles depuis des paquets NPM,
facilitant la réutilisation et la modularisation des règles.
Il comprend également une passe d'optimisation dite de [_constant
folding_](https://en.wikipedia.org/wiki/Constant_folding) pour pré-calculer les
règles à la compilation, réduisant ainsi significativement le temps de calcul
côté client et la consommation de bande passante.

Le support de la présentation que j'ai donné sur l'utilisation de ce paquet au
sein de [Nos Gestes Climat](/?=nos-gestes-climat) est diponible
[ici](https://github.com/publicodes/tools/blob/main/presentation-2023-11-30/pres.pdf).

### Extension VSCode

Je suis le créateur et le mainteneur du paquet
[`publicodes/language-server`](https://github.com/publicodes/language-server).
Cette extension VSCode et _language server_ permettent d'avoir une expérience
de développement avec Publicodes comme avec un langage de programmation
classique, avec des fonctionnalités telles que l'autocomplétion, les
diagnostics, les _gotos def_, etc...

---

## Ressources

### Site web

- [publi.codes](https://publi.codes)
- [publicodes.github.io/tools](https://publicodes.github.io/tools)

### Présentation

- [publicodes/tools/presentation-2023-11-30](https://github.com/publicodes/tools/blob/main/presentation-2023-11-30/pres.pdf)

### Code source

- [publicodes](https://github.com/publicodes/publicodes)
- [publicodes/tools](https://github.com/publicodes/tools)
- [publicodes/language-server](https://github.com/publicodes/language-server)
- [publicodes/model-template](https://github.com/publicodes/model-template)
"""


codeSnippet : CodeSnippet.Infos
codeSnippet =
    { file = "publicodes/tools/source/optims/constantFolding.ts"
    , href = "https://github.com/publicodes/tools/blob/abf3501717d8a297ad79b0abde73f85bb7f21804/source/optims/constantFolding.ts#L187-L217"
    , code =
        """function searchAndReplaceConstantValueInParentRefs(
  ctx: FoldingCtx,
  ruleName: RuleName,
  constantNode: ASTNode,
) {
  const refs = ctx.refs.parents.get(ruleName)


  if (refs) {
    for (const parentName of refs) {
      const parentRule = ctx.parsedRules[parentName]


      if (!ctx.params.toAvoid?.(parentRule)) {
        const newRule = traverseASTNode(
          transformAST((node, _) => {
            if (node.nodeKind === 'reference' && node.dottedName === ruleName) {
              return constantNode
            }
          }),
          parentRule,
        ) as RuleNode


        if (newRule !== undefined) {
          ctx.parsedRules[parentName] = newRule
          ctx.parsedRules[parentName].rawNode[ctx.params.isFoldedAttr] =
            'partially'
          removeInMap(ctx.refs.parents, ruleName, parentName)
        }
      }
    }
  }
}
        """
    }
