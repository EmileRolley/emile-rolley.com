module Projects.Infos exposing (..)

import CodeSnippet


type alias ProjectInfos =
    { name : String
    , body : String
    , codeSnippet : CodeSnippet.Infos
    }
