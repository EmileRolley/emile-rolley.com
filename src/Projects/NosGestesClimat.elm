module Projects.NosGestesClimat exposing (infos)

import CodeSnippet
import Projects.Infos exposing (ProjectInfos)


infos : ProjectInfos
infos =
    { name = "Nos\u{00A0}Gestes\u{00A0}Climat"
    , body = body
    , codeSnippet = codeSnippet
    }


body : String
body =
    """
[Nos Gestes Climat](https://nosgestesclimat.fr) est le simulateur
d'empreinte carbone individuel de l'[Agence de la Transition
Écologique](https://www.ademe.fr/) (ADEME). Il permet de calculer gratuitement
et en quelques minutes son empreinte carbone personnelle et découvrir les
gestes ayant le plus d'impact pour la réduire.

Le simulateur est open-source et repose sur un modèle de calcul implémenté en
[Publicodes](/?=publicodes) offrant ainsi une [documentation
intéractive et personnalisée](https://nosgestesclimat.fr/documentation/bilan)
pour chaque simulations, ce qui facilite la compréhension des résultats
et accroît la transparence des calculs.

## Contributions

J'ai contribué au projet en tant que développeur indépendant de septembre 2022
à février 2024.

### Amélioration du DX

Ma principale mission a été de développer et maintenir un ensemble d'outils
facilitant le développement, le test et la maintenance du simulateur NGC.

Cela a inclus la mise en place d'une suite de tests automatisés aussi bien côté
modèle avec un ensemble de scripts de test de régression, que côté site avec
des tests end-to-end.
J'ai également implémenté une application dédiée au développement local du
modèle, automatisant la compilation des différentes parties, les tests de
non-régression, et l'exploration de la documentation en fonction de profils
types.

### Publicodes

[Publicodes](/?=publicodes) est un langage de modélisation de calculs
permettant de définir facilement un ensemble de règles de calcul élémentaires
et de les combiner pour obtenir des résultats plus complexes.

J'ai contribué à l'amélioration de l'écostystème en développant un jeu d'outils
[`@publicodes/tools`](https://github.com/publicodes/tools). Cela a permis la
réutilisation de règles de calculs grâce à un système d'import et l'application
d'une passe d'optimisation réduisant ainsi la taille du modèle compilé, le
temps de calcul côté client et la consommation de bande passante.

### Traduction et internationalisation

J'ai également mis en place l'architecture technique permettant la traduction
de l'interface du site ainsi que l'internationalisation du modèle de calcul en
[20 déclinaisons régionales](https://nosgestesclimat.fr/international).

---

## Ressources

### Site web

- [nosgestesclimat.fr](https://nosgestesclimat.fr)
- [nosgestesclimat.fr/documentation](https://nosgestesclimat.fr/documentation/bilan)
- [publi.codes](https://publi.codes)

### Code source

- [incubateur-ademe/nosgestesclimat](https://github.com/incubateur-ademe/nosgestesclimat)
- [incubateur-ademe/nosgestesclimat-site-nextjs](https://github.com/incubateur-ademe/nosgestesclimat-site-nextjs)
- [publicodes/tools](https://github.com/publicodes/publicodes/tools)
- [publicodes](https://github.com/publicodes/publicodes)
"""


codeSnippet : CodeSnippet.Infos
codeSnippet =
    { file = "nosgestesclimat/data/transport/vacances.publicodes"
    , href = "https://github.com/incubateur-ademe/nosgestesclimat/blob/9aab52437ef403ecc6dd8890720b6fcf21ac0ba9/data/transport/vacances.publicodes#L116C1-L155C51"
    , code =
        """transport . vacances . camping car:
  titre: Camping Car
  icônes: 🚐
  applicable si:
    toutes ces conditions:
      - km > 0
      - propriétaire
  formule: usage réel + construction amortie

transport . vacances . camping car . propriétaire:
  question: Possédez-vous un camping car ?
  par défaut: non

transport . vacances . camping car . usage réel:
  titre: Usage Camping Car
  formule: usage / logement . habitants
  unité: kgCO2e

transport . vacances . camping car . construction amortie:
  titre: Construction du camping car
  formule: (construction / durée de vie) / logement . habitants
  unité: kgCO2e

transport . vacances . camping car . construction:
  formule:
    produit:
      - voiture . gabarit . berline . empreinte
      - (camping car . poids / voiture . gabarit . berline . poids)
  unité: kgCO2e
  note: |
    On extrapole l'impact de la fabrication d'une caravane car à partir d'un
    ratio sur le poids et en considérant les données de construction "Berline"
    (6300 kgCO2e et 1500 kg)...

transport . vacances . camping car . poids:
  formule: 3000
  unité: kg

transport . vacances . camping car . durée de vie:
  formule: 25
  note: |
    On considère une durée de vie de [25
    ans](https://objectifpleinair.com/combien-de-temps-dure-un-camping-car/).
    Cette hypothèse nécessite d'être mieux sourcée
    """
    }
