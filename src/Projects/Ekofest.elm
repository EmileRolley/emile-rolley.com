module Projects.Ekofest exposing (infos)

import CodeSnippet
import Projects.Infos exposing (ProjectInfos)


infos : ProjectInfos
infos =
    { name = "Ekofest"
    , body = body
    , codeSnippet = codeSnippet
    }


body : String
body =
    """
[Ekofest](https://ekofest.fr) a pour objectif de faciliter l'organisation
d'événements festifs et culturels éco-responsables. L'outil permet de
rapidement estimer l'impact carbone d'un événement afin de repérer les postes
les plus émetteurs et anticiper les actions à mettre en place.

Ce simulateur a été développé dans une démarche de transparence et de partage.
Ainsi, le [code source du simulateur](https://github.com/ekofest/ekofest) est
libre et ouvert, de la même manière que le [modèle de
calcul](https://ekofest.github.io/publicodes-evenements/doc/resultats). Le
simulateur est complètement gratuit et ne nécessite aucun compte utilisateur.

## Contributions

J'ai co-créé ce projet avec [Clément Auger](https://github.com/Clemog) en
février 2024.

Le projet est en cours de développement et je m'occupe principalement du
développement front-end de l'application en [Elm](https://elm-lang.org/) qui
utilise le [modèle de calcul](https://github.com/ekofest/publicodes-evenements)
rédigé par Clément en [Publicodes](https://publi.codes).

Mon implication dans ce projet est bénévole.

---

## Ressources

### Site web

- [ekofest.fr](https://ekofest.fr)
- [ekofest.github.io/publicodes-evenements](https://ekofest.github.io/publicodes-evenements)
- [publicodes](https://publicodes.fr)

### Code source

- [ekofest/ekofest](https://github.com/ekofest/ekofest)
- [ekofest/publicodes-evenements](https://github.com/ekofest/publicodes-evenements)
- [Publicodes](https://github.com/publicodes/publicodes)
"""


codeSnippet : CodeSnippet.Infos
codeSnippet =
    { file = "ekofest/src/Main.elm"
    , href = "https://github.com/ekofest/ekofest/blob/28f78005e0de6307ec03ea93c5100c97a82e942c/src/Main.elm#L623-L664"
    , code =
        """viewInput : Model -> ( P.RuleName, P.RawRule ) -> Bool -> Html Msg
    viewInput model ( name, rule ) isApplicable =
        let
            newAnswer val =
                case String.toFloat val of
                    Just value ->
                        NewAnswer ( name, P.Num value )

                    Nothing ->
                        if String.isEmpty val then
                            NoOp

                        else
                            NewAnswer ( name, P.Str val )

            maybeNodeValue =
                Dict.get name model.evaluations
                    |> Maybe.map .nodeValue
        in
        if not isApplicable then
            viewDisabledInput

        else
            case ( ( rule.formula, rule.unit ), maybeNodeValue ) of
                ( ( Just (UnePossibilite { possibilites }), _ ), Just nodeValue ) ->
                    viewSelectInput model.rawRules name possibilites nodeValue

                ( ( _, Just "%" ), Just (P.Num num) ) ->
                    viewRangeInput num newAnswer

                ( _, Just (P.Num num) ) ->
                    viewNumberInputOnlyPlaceHolder num newAnswer

                ( _, Just (P.Boolean bool) ) ->
                    viewBooleanRadioInput name bool

                _ ->
                    viewDisabledInput
    """
    }
