module CodeSnippet exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import SyntaxHighlight exposing (noLang, toBlockHtml)


type alias Infos =
    { file : String
    , href : String
    , code : String
    }


view : Infos -> Html msg
view infos =
    div
        [ class "text-md" ]
        [ div [ class "text-gray-100 font-mono font-bold mb-1 hover:text-pink-400" ]
            [ a [ href infos.href, target "_blank", class "flex pb-2 mb-2 items-center border-b border-gray-600" ]
                [ text infos.file
                ]
            ]
        , div [ class "text-slate-50 overflow-x-auto" ]
            [ noLang infos.code
                |> Result.map (toBlockHtml (Just 1))
                |> Result.withDefault
                    (pre [] [ code [ class "" ] [ text infos.code ] ])
            ]
        ]
