// @ts-ignore
import { Elm } from "./Main.elm";

const app = Elm.Main.init({
  node: document.getElementById("elm-app"),
});

app.ports.simpleScrollTo.subscribe((id) => {
  const element = document.getElementById(id);
  console.log("scrolling to", element, id);
  if (element) {
    window.scroll({
      // NOTE: only way to work properly but with 1px offset difference when sticked to top-0
      // TODO: find a better way to scroll to the top of the element
      top: element.offsetTop - 1,
      left: 0,
      behavior: "instant",
    });
  }
});
